import axios from 'axios';

axios.defaults.withCredentials = false;

export const API = axios.create({
  baseURL: 'http://localhost:9000/api',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const test = () => 'test';
