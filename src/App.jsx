/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { login, logout } from './slice/user.slice';

export default function App() {
  const { user, error } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    username: '',
    password: '',
  });

  const handleLogin = (e) => {
    e.preventDefault();
    dispatch(login(form));
  };

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <div>
      <form onSubmit={handleLogin}>
        <div>
          <label>Username</label>
          <input type="text" name="username" value={form.username} onChange={(e) => setForm({ ...form, username: e.target.value })} required />
        </div>
        <div>
          <label>Password</label>
          <input type="password" name="password" value={form.password} onChange={(e) => setForm({ ...form, password: e.target.value })} required />
        </div>
        {error && (
          <div>
            {error.message}
          </div>
        )}
        <div>
          <button type="submit">Enviar</button>
        </div>
      </form>
      { user && (
        <div>
          <h1>
            Welcome
            {' '}
            {user}
          </h1>
          <button type="button" onClick={handleLogout}>Logout</button>
        </div>
      )}
    </div>
  );
}
