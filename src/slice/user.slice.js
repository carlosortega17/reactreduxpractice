/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import { API } from '../util/func';

const slice = createSlice({
  name: 'user',
  initialState: {
    user: null,
    error: null,
  },
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload.user;
      state.error = null;
    },
    onError: (state, action) => {
      state.error = action.payload.error;
    },
    setLogout: (state) => {
      state.user = null;
      state.error = null;
    },
  },
});

const {
  setUser,
  onError,
  setLogout,
} = slice.actions;

export const login = ({ username, password }) => (dispatch) => {
  API.post('/login', { username, password }).then(({ data }) => {
    const { userInfo: { name } } = data;
    dispatch(setUser({ user: name }));
  }).catch(({ response }) => {
    const error = response.data;
    dispatch(onError({ error }));
  });
};

export const logout = () => (dispatch) => {
  dispatch(setLogout());
};

export default slice.reducer;
