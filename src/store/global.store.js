import { combineReducers } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import userSlice from '../slice/user.slice';

const reducer = combineReducers({
  users: userSlice,
});

const store = configureStore({
  reducer,
});

export default store;
