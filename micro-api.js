const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors({ origin: '*' }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api', (req, res) => {
  res.json({
    name: 'test',
  });
});

app.post('/api/login', (req, res) => {
  const { body } = req;
  if (body && body.username && body.password) {
    const { username, password } = body;
    if (username === 'test' && password === '123456') {
      res.json({
        id: 1,
        userInfo: {
          name: 'guest',
          rol: 'user',
        },
      });
    } else {
      res.status(401).json({
        name: 'unauthorized',
        message: 'User or password error',
      });
    }
  } else {
    res.status(400).json({
      name: 'badRequest',
      message: 'Bad request',
    });
  }
});

app.listen(9000);
